//
//  ContentView.swift
//  Preparation
//
//  Created by itisioslab on 12.05.2021.
//

import SwiftUI

struct ContentView: View {
    @State var firstCard = 2
    @State var secondCard = 3
    @State var playerScore = 0
    @State var CPUScore = 0
    var body: some View {
        ZStack {
            Image("background").ignoresSafeArea()
            VStack {
                Spacer()
                Image("logo")
                Spacer()
                HStack {
                    Spacer()
                    Image("card\(firstCard)")
                    Spacer()
                    Image("card\(secondCard)")
                    Spacer()
                }
                Spacer()
                Button(action: {
                    firstCard = Int.random(in: 2...14)
                    secondCard = Int.random(in: 2...14)
                    if firstCard > secondCard {
                        playerScore += firstCard
                    } else {
                        CPUScore += secondCard
                    }
                }, label: {
                    Image("dealbutton")
                })
                Spacer()
                HStack {
                    Spacer()
                    VStack {
                        Text("Player")
                            .font(.headline)
                            .foregroundColor(Color.white)
                        Text("\(playerScore)").foregroundColor(Color.white)
                    }
                    Spacer()
                    VStack {
                        Text("CPU")
                            .font(.headline).foregroundColor(Color.white)
                        Text("\(CPUScore)").foregroundColor(Color.white)
                    }
                    Spacer()
                }
                Spacer()
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
