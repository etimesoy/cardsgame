//
//  CardsGameApp.swift
//  CardsGame
//
//  Created by itisioslab on 13.05.2021.
//

import SwiftUI

@main
struct CardsGameApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
