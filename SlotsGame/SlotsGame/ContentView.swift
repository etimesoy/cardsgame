//
//  ContentView.swift
//  SlotsGame
//
//  Created by itisioslab on 17.05.2021.
//

import SwiftUI

struct ContentView: View {
    @State var creditsCount = 1000
    var availableSlots = ["apple", "cherry", "star"]
    @State var actualSlots = ["apple", "cherry", "star"]
    @State var threeInARow = false
    var body: some View {
        VStack {
            Spacer()
                .frame(height: 35.0)
            Text("SwiftUI Slots!")
                .font(.largeTitle)
            Spacer()
            Text("Credits: \(creditsCount)")
            Group {
                Spacer()
                    .frame(height: 50)
                if threeInARow {
                    Text("Three in a row!")
                        .font(.title)
                }
                Spacer()
                    .frame(height: 80)
            }
            HStack() {
                Spacer()
                Image(actualSlots[0])
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                Spacer()
                Image(actualSlots[1])
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                Spacer()
                Image(actualSlots[2])
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                Spacer()
            }
            Spacer()
            Button(action: {
                for i in 0...2 {
                    actualSlots[i] = availableSlots[Int.random(in: 0...2)]
                }
                if actualSlots[0] == actualSlots[1] && actualSlots[1] == actualSlots[2] {
                    creditsCount += 50
                    threeInARow = true
                } else {
                    creditsCount -= 5
                    threeInARow = false
                }
            }, label: {
                Text("Spin").foregroundColor(Color.white).padding().frame(width: 125.0).background(Color(.systemPink)).cornerRadius(25).fixedSize(horizontal: true, vertical: false)
            })
            Spacer()
                .frame(height: 90.0)
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
