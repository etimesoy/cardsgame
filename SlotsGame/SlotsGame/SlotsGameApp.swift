//
//  SlotsGameApp.swift
//  SlotsGame
//
//  Created by itisioslab on 17.05.2021.
//

import SwiftUI

@main
struct SlotsGameApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
